import boto3
import clear_term
import click




def vol_clean(reg,amc):
    ec2_con2=amc.resource(service_name='ec2',region_name=reg)
    filter_ebs_unsed={"Name":"status", "Values":["available"]}
    volume = ec2_con2.volumes.filter(Filters=[filter_ebs_unsed])   # Filtering unattached volumes
    c=0
    [print("\nFollowing volumes are not in used :- \n(Verify before proceed)    \n")]
    for each in volume:
        c=1
        if not each.tags:                       # Filtering unstagged volumes
            print("\t",c,".  Volume id:-",each.id,"   State :- ",each.state,"  Tags:-",each.tags)
            c+=1
            confirm=input("\nAre you sure about deleting unsed Volumes (Press Y or Enter to exit ):- ")
            if confirm.lower() == 'y' or confirm == 'yes':
                print("\nDeleting.....",each.id,"   State :- ",each.state)
                each.delete()
            else:
                print("\n\nExiting.........")
    if c==0:
        print("\tGreat No untagged and unused volume/s present for cleanup ")
    input("\nPress Enter to continue.....\n\n")
    
    return None


def main():
    amc=boto3.session.Session(profile_name='tanmay')
    region = click.prompt("Enter the AWS Region, Default is ", type=str, default='ap-south-1')
    vol_clean(region,amc)
    return None

if __name__=="__main__":
    main()