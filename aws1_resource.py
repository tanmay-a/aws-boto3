'''
=================Author====================
Name:- Tanmay
email:- tanmay.vn@gmail.com
https://www.linkedin.com/in/tanmay-70336398/

Note:-
Simple script to get list of all AWS users/groups/roles ,ec2,sg,vpc,s3 bucket using boto3
Make sure AWS CLI is installed and aws cred. is configured.
Install Click "pip3 install click"
Step1:- Get AWS Management console
Step2:- Get IAM console
Step3:- Get S3 console
'''

import clear_term
import boto3,csv
import click
from pprint import pprint

class Aws_details(object):
    def __init__(self):
        pass
    # def __init__(self,amc,region):
    #     self.display_iam(amc)
    #     self.display_s3(amc)
    #     self.display_ec2(amc,region)

    def display_iam(self,con):
        self.iamc=con.resource('iam')         #Creating IAM console object using resource method
        print("\nAll existing users are :")
        for each in self.iamc.users.all():   # Read the users,groups,roles
            print(each.name)

        print("\nAll Existing groups are :")
        for each in self.iamc.groups.all():
            print(each.name)

        print("\nAll Existing roles are :")
        for each in self.iamc.roles.all():
            print(each.name)
        return None

    def display_ec2(self,con,reg):
        z=1
        self.reg=reg
        self.ec2c=con.resource(service_name='ec2',region_name=self.reg)
        print(f"\n\n\nAll Existing EC2 in {self.reg} region is\\are :\n")
        #pprint(self.ec2c.instances.all())
        export=input(" Do you want EC2 instance data export as CSV  (Press Y or Yes)  :")
        export=export.lower()
        if export == 'y' or export == 'yes':
            new_file="ec2-export.csv"
            fo=open(new_file,'w')
            wo=csv.writer(fo,delimiter=',')
            data=(['EC2 Instance Name(tag)','Attached VPC ID','State','Public IP','Private IP','Subnet ID','Intance Type','Security Group Id'])
            wo.writerow(data)
        for i in self.ec2c.instances.all():
            print(f"\n\n##################################{z}##########################################\n")
            z+=1
            for tag in i.tags:
                if tag['Key'] == 'Name':
                    print("EC2 Instance Name(tag) :",tag['Value'])   
                    name_tag=tag['Value']
            print("Attached VPC ID: ",i.vpc.id)
            print("State: ",i.state['Name'])
            print("Public IP: ",i.public_ip_address)
            print("Private IP: ",i.private_ip_address)
            print("Subnet ID: ",i.subnet_id)
            print("Intance Type: ",i.instance_type)
            print("Security Group Name:",i.security_groups[0]['GroupName'],"Security Group Id:-",i.security_groups[0]['GroupId'])
            if export == 'y' or export == 'yes':
                data2=([name_tag,i.vpc.id,i.state['Name'],i.public_ip_address,i.private_ip_address,i.subnet_id,i.instance_type,i.security_groups[0]['GroupId']])
                wo.writerow(data2)
        self.vpc=self.ec2c.vpcs.all()
        print(f"\n\n\n\nAll Existing VPC in {self.reg} region is\\are :\n")
        z=1
        for each_vpc in self.vpc:
            print(f"\n\n##################################{z}##########################################\n")
            z+=1
            try:
                print(each_vpc.tags[0]['Value']) # NEED TO CHECK
            except Exception as e:
                print("VPC name is not specified")
            print(each_vpc.vpc_id)
            print(each_vpc.cidr_block)
            print(each_vpc.state)
        self.sg = self.ec2c.security_groups.all()
        print(f"\n\n\nAll Existing Security Group in {self.reg} region is\\are :\n")
        z=1
        for each_sg in self.sg:
            print(f"\n\n##################################{z}##########################################\n")
            z+=1
            print("Security Group ID:- ",each_sg.id,'\n')
            if each_sg.description == 'default VPC security group':
                print(f"This is the {each_sg.description} allowing connection from itself attached objects")
            else:
                c=1
                print("Ingress Rules=======================")
                #pprint(each_sg.ip_permissions)
                for sgi in each_sg.ip_permissions:
                    print(f"{c}:-")
                    try:
                        print(f"\tAllowed Ports from {sgi['FromPort']} to {sgi['ToPort']}")
                    except Exception as e:
                        print(f"\tAllowed security group",sgi['UserIdGroupPairs'])
                    print("\tAllowed Protocol:- ",sgi['IpProtocol'])
                    if sgi['IpRanges']:
                        print("\tAllowed IP-Range",sgi['IpRanges'])
                    else:
                        print(f"\tAllowed security group",sgi['UserIdGroupPairs'])
                    c+=1
                print("\nEgress Rules========================")
                c=1
                #pprint(each_sg.ip_permissions_egress)
                for sge in each_sg.ip_permissions_egress:
                    print(f"{c}:-")
                    if sge['IpProtocol'] == '-1' and sge['IpRanges'][0]['CidrIp'] == '0.0.0.0/0' :
                        print("\tAll Outgoing connections are open")
                    else:
                        print(f"\tAllowed Ports from {sge['FromPort']} to {sge['ToPort']} ")
                        print("\tAllowed Protocol:- ",sge['IpProtocol'])
                        print("\tAllowed IP-Range",sge['IpRanges'])
                    c+=1

    def display_s3(self,con):
        self.s3c=con.resource('s3')           #Creating S3 console object using resource method
        print("\n\n\nAll Existing S3 buckets are :")
        for each in self.s3c.buckets.all():
            print(each.name)
        return None

def main():
    amc = boto3.session.Session(profile_name="tanmay")  #create a varibale amc for login to Amazon Management Console
    region_name = click.prompt("Enter the AWS Region, Default is ", type=str, default='ap-south-1')
    #Aws_details(amc,region_name)
    ob=Aws_details()
    ob.display_iam(amc)
    ob.display_ec2(amc,region_name)
    ob.display_s3(amc)

if __name__=='__main__':
    main()