'''
=================Author====================
Name:- Tanmay
email:- tanmay.vn@gmail.com
https://www.linkedin.com/in/tanmay-70336398/
'''
import clear_term,boto3,click,sys,time
from pprint import pprint
from aws6_VolClean import vol_clean                           # Importing from another script
from aws1_resource import Aws_details


def list_ec2(reg,amc):
    ec2_c=amc.client(service_name='ec2',region_name=reg)
    z_c=ec2_c.describe_instances()
    ec2_list=[]
    for each_ec2 in z_c['Reservations']:
        for each_ins in each_ec2['Instances']:
            ec2_list.append(each_ins['InstanceId'])
    return ec2_list   
def print_ec2(reg,amc):
    ec2_con=amc.client(service_name='ec2',region_name=reg)
    z=ec2_con.describe_instances()
    c=1
    for each_ins in z['Reservations']:
        for each_ob in each_ins['Instances']:
            print(f"EC2 Instance ID-{c} is : {each_ob['InstanceId']} and State = {each_ob['State']['Name']} and Tags are:- ")
            for tag in each_ob['Tags']:
                print("\t\t",tag)
            c+=1
            print("---------------------------------------------------------")
    input("\n\nPress Enter to continue............")
    return None
def stop_ec2(reg,amc):
    ec2_con=amc.client(service_name='ec2',region_name=reg)
    ec2id=input("Enter the Instance id to stop\n(You can get the ID from main menu option-1) : ")
    s=list_ec2(reg,amc)
    p=ec2id in s
    if p == False:
        print("The instance id is wrong or not present in the selected region")
        stop_ec2(reg,amc)
    else:   
        confirm=input("Are you sure about stopping the instance(Press Y or Yes )")
        if confirm.lower() == 'y' or confirm == 'yes':
            print("\nStopping the Instance.....Please wait...\n\n")
            time.sleep(2)
            ec2_con.stop_instances(InstanceIds=[ec2id])
            waiter=ec2_con.get_waiter('instance_stopped')
            waiter.wait(InstanceIds=[ec2id])
            print(f"\nGreat, The requested Instance is successfully stopped, The current state code & Name is :- \n")
            state=ec2_con.describe_instances(InstanceIds=[ec2id])
            print(state['Reservations'][0]['Instances'][0]['State'])
        else:
            print("\n\nPlease choose the correct option\n")
            stop_ec2(reg,amc)
        input("\n\nPress Enter to continue.................")
        return None
def start_ec2(reg,amc):
    ec2_con=amc.client(service_name='ec2',region_name=reg)
    ec2id=input("Enter the Instance id to stop\n(You can get the ID from main menu option-1) : ")
    s=list_ec2(reg,amc)
    p=ec2id in s
    if p == False:
        print("The instance id is wrong or not present in the selected region")
        start_ec2(reg,amc)
    else:   
        confirm=input("\n\nAre you sure about starting the instance(Press Y or Yes )")
        if confirm.lower() == 'y' or confirm == 'yes':
            print("\n\nstarting the Instance.....Please wait...\n\n")
            ec2_con.start_instances(InstanceIds=[ec2id])
            waiter=ec2_con.get_waiter('instance_running')
            waiter.wait(InstanceIds=[ec2id])    # 40 Checks after every 15 seconds only in client waiter
            print(f"\nGreat, The requested Instance is successfully started, The current state is :- \n")
            z=ec2_con.describe_instance_status(InstanceIds=[ec2id])
            print(z['InstanceStatuses'][0]['InstanceState'])
        else:
            print("\n\nPlease choose the correct option\n")
            start_ec2(reg,amc)
        input("\n\nPress Enter to continue............")
        return None
def stop_all(reg,amc):
    s=list_ec2(reg,amc)
    ec2_con=amc.client(service_name='ec2',region_name=reg)
    confirm=input("Are you sure about stopping all the instance(Press Y or Yes )")
    if confirm.lower() == 'y' or confirm == 'yes':
        print("Stopping All the Instance.....Please wait...\n\n")
        #print(s)
        ec2_con.stop_instances(InstanceIds=s)
        waiter=ec2_con.get_waiter('instance_stopped')
        waiter.wait(InstanceIds=s)
        print("All Ec2 Machines listed below have been stopped \n ")
        print_ec2(reg,amc)
    else:
        print("\n\nPlease choose the correct option\n")
        stop_all(reg,amc)
    input("\n\nPress Enter to continue...........")
    return None
def start_all(reg,amc):
    s=list_ec2(reg,amc)
    ec2_con=amc.client(service_name='ec2',region_name=reg)
    confirm=input("Are you sure about starting all the instance at once : (Press Y or Yes )")
    if confirm.lower() == 'y' or confirm == 'yes':
        print("Starting All the EC2 Instance.....Please wait...\n\n")
        #print(dir(ec2_con.instances))
        #ec2_con.instances.start()
        ec2_con.start_instances(InstanceIds=s)
        waiter=ec2_con.get_waiter('instance_running')
        waiter.wait(InstanceIds=s)
        print("All Ec2 Machines listed below have been started :-\n\n")
        print_ec2(reg,amc)
    else:
        print("\n\nPlease choose the correct option\n")
        start_all(reg,amc)
    input("\n\nPress Enter to continue..........")
    return None
def ec2_tag(reg,amc):
    ec2_con=amc.resource(service_name='ec2',region_name=reg)
    key=str(input("Enter the tag key for filteration :  "))
    key="tag:"+key
    value=[]
    z=input("Enter the tag key Value :  ")
    value.append(z)
    f1={'Name': key,'Values': value}
    f2={'Name': 'instance-state-name','Values': ['running','stopped']}
    instance_iterator = ec2_con.instances.filter(Filters=[f1,f2])
    print("\n\n Listing all the instances with specific tags.........:- \n")
    for each in instance_iterator:
        print(each.id)
    input("\n\nPress Enter to continue..........")
    return None

def main():
    amc=boto3.session.Session(profile_name='tanmay')
    region = click.prompt("Enter the AWS Region, Default is ", type=str, default='ap-south-1')
    while True:
        print(f"\n\nThe Following operations are supported for EC2 on {region} :- ")
        print('''

            1. List All EC2 with ID, State & Tags
            2. Filter EC2 with specific tag (Ready with your key and value) 
            3. Start single Machine
            4. Stop single Machine
            5. Start All
            6. Stop All
            7. Display IAM User,Groups,roles,EC2-Instance,VPC,Security group and CSV Export  
            8. Volume Cleanup (Untagged and Unused)
            9. Exit this Menu

            Note:- More Functionality are on the way
            ''')
        opt=click.prompt("Please select below function to proceed:    Default:", type=int, default=9)
        if opt == 1:
            print("Listing all the instances.........:- \n")
            print_ec2(region,amc)
        elif opt == 2:
            ec2_tag(region,amc)
        elif opt == 3:
            print("Starting the Instance..................:- ")
            start_ec2(region,amc)
        elif opt == 4:
            print("You have choosed to stop an ec2 instance.........:- \n\n")
            stop_ec2(region,amc)
        elif opt == 5:
            print("Starting all the Instance.........:- ")
            start_all(region,amc)
        elif opt == 6:
            print("Stopping all the Instance.........:- ")
            stop_all(region,amc)
        elif opt == 7:
            Aws_details().display_ec2(amc,region)
        elif opt == 8:
            vol_clean(region,amc)
        elif opt == 9:
            print("Exiting...........................:-\n\n ")
            time.sleep(1)
            sys.exit()
        else:
            print("\nYou have entered wrong choice, Please select valid options ")
    return None

if __name__=="__main__":
    main()