'''
=================Author====================
Name:- Tanmay
email:- tanmay.vn@gmail.com
https://www.linkedin.com/in/tanmay-70336398/


Note:-
# In last example we have used resource object to display the available resources on your aws account but resource does support
# only limited number of objects i.e ec2,s3,cloudformation,sns,sqs,glacier,iam,cloudwatch.
# Rest all services needs to be work with client. CLient is low level where output is in dictationary format means not formatted
# whereas resource is a high level object but with limited functionality and clear output.
'''


import clear_term
import boto3

#Below step is required only when you want to create a custom session, Otherwise boto3 will use the default session.
# opt=amc.get_available_resources()
# print(opt)
print("############# Using Client and Custom session ##############")
amc=boto3.session.Session(profile_name="tanmay")
iamc_client=amc.client(service_name='iam')
#print(iamc.list_users()['Users'])
for each in iamc_client.list_users()['Users']:
    print(each['UserName'])

print("\n############# Using Resource and default session ############")
iamc_res=boto3.resource(service_name='iam')
for each in iamc_res.users.all():
    print(each.name)

print("\n############# Using Client and list ec2 ############")
ec2_client=boto3.client(service_name='ec2',region_name='ap-south-1')
z=ec2_client.describe_instances()
c=1
for each_ins in z['Reservations']:
    #print(each_ins['Instances'])
    for each_ob in each_ins['Instances']:
        print(f"EC2 Instance ID-{c} is : {each_ob['InstanceId']} and State = {each_ob['State']['Name']} and Tags are:- ")
        for tag in each_ob['Tags']:
            print(tag)
        #  \n{each_ob['Tags'][0]}\n{each_ob['Tags'][1]} \n\n")
        c+=1
        print("---------------------------------------------------------")

print("\n############# Using Client and list bucket ############")

s3_client=boto3.client(service_name='s3')
# s3_list=boto3.client(s)
# print(s3_client)
# print(s3_client['Owner'])
for buc in s3_client.list_buckets()['Buckets']:
    z=buc['Name']
    print("################################",'\n')
    print(f"Bucket name is {z} and available objects is/are :- ")
    for ob in s3_client.list_objects(Bucket=z)['Contents']:
        print(ob['Key'])

print("#########Donwloading file from s3 ######################")
s3 = boto3.resource('s3')
s3.meta.client.download_file('tf-ansible-bucket-1', 'tfstatefile', './tfstatefile')