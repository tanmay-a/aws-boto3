'''
=================Author====================
Name:- Tanmay
email:- tanmay.vn@gmail.com
https://www.linkedin.com/in/tanmay-70336398/

Note:-
# Here we will describe how to enter to client type object using 'meta' if you have already created a resrouce type object.
# No need to again change the complete script to client type
# i.e.Describes the EC2 Regions that are enabled for your account, or all Regions.
'''

import clear_term,click,os,boto3
import aws6_VolClean
from aws1_resource import Aws_details

amc=boto3.session.Session(profile_name='tanmay')

'''
ec2_con=amc.resource(service_name='ec2')
#print(dir(ec2_con.meta.client))  # Use this to list out all the available methods
#print(ec2_con.meta.client.describe_regions(AllRegions=True))   #including Regions that are disabled for your account
#print(ec2_con.meta.client.describe_regions()['Regions'])
for i in ec2_con.meta.client.describe_regions()['Regions']:
    print(i)


# Lets do something more with filters using collection
input("Press Enter to continue...")
print("\n\n\n############################Collection Usage#####################################")
print("#####################Showing EC2 using filter#####################################\n\n")
ec2_con1=amc.resource(service_name='ec2',region_name='ap-south-1')
instance_iterator = ec2_con1.instances.filter(Filters=[{'Name':'tag:Cost Center','Values': ['CloudOps']}])
for each in instance_iterator:
    print(each.id)
input("Press Enter to continue...")

'''


#aws_VolClean6.vol_clean('ap-south-1',amc)
region_name = click.prompt("Enter the AWS Region, Default is ", type=str, default='ap-south-1')
Aws_details().display_ec2(amc,region_name)