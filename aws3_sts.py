'''
=================Author====================
Name:- Tanmay
email:- tanmay.vn@gmail.com
https://www.linkedin.com/in/tanmay-70336398/

Note:-
#This script help to find the aws account id where we are working currenty.
#This could be useful when we are working with multiple aws account
'''

import clear_term
import boto3
from pprint import pprint 

amc=boto3.session.Session(profile_name="tanmay")
stsc=amc.client('sts')
print("AWS Account : ",stsc.get_caller_identity()['Account'],"\n")
#pprint(stsc.get_caller_identity())
ec2c = amc.client(service_name='ec2',region_name='ap-south-1')
con = ec2c.describe_instances().get('Reservations')
for each in con:
    for i in each['Instances']:
        #pprint(i)
        print(i['InstanceId'])
        # print(i['PrivateDnsName'])
        # print(i['InstanceType'])
        # print(i['PublicIpAddress'])
        # print(i['LaunchTime'].strftime("%d %B, %Y"))
        # print(i['KeyName'])
        # print("##################################")
stopec2=ec2c.stop_instances(InstanceIds=['i-004fe7222de2e25e2'])
pprint(stopec2)